# Project 27-Media-Player

Media player which supports many video formats, and allows users to annotate the video.

## Short Description

Simple video player. It has most common features like, speed configuring, fast fowarding, playlists, subtitles etc..
It also allows users to add and save annotations in time intervals.

## Requirements:

Project is done in qt framework, so in order for this app to be built it requires minimum version of qt 5.7.

## Demo 
[Video demostration](https://drive.google.com/file/d/1VmzYWPajRwyFmt_xqjiwSDAMaMKdJfHH/view?usp=sharing)

## Developers

- [Mladen Dilparić, 201/2015](https://gitlab.com/giomla93)
- [Sara Kapetinić, 182/2017](https://gitlab.com/SaraKapetinic_mi17182)
- [Filip Nedeljković, 305/2017](https://gitlab.com/efen16)
- [Nikola Panić, 284/2017](https://gitlab.com/mrpannic)
- [Nikola Nikolić, 35/2015](https://gitlab.com/nnikolic1)
